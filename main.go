package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"github.com/dustin/go-humanize"
)

const (
	TAX_RATE = 0.25
)

type Entry struct {
	User   string
	Type   string
	Volume float64
	Value  int
}

type Person struct {
	Name   string
	Volume float64
	Value  int
}

func main() {
	var path = flag.String("source", "", "Path to file")
	flag.Parse()

	if *path == "" {
		fmt.Println("Filename not provided")
		os.Exit(1)
	}

	file, err := os.Open(*path)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	var parsed []Entry
	scanner := bufio.NewScanner(file)
	var iterator int
	for scanner.Scan() {
		iterator++
		//Skip header
		if iterator == 1 {
			continue
		}
		line := scanner.Text()
		fields := strings.Split(line, "    ")
		//1 - date
		//2 - corp
		//3 - user
		//4 - type
		//5 - volume
		//6 - price

		volume, err := strconv.ParseFloat(fields[5], 64)
		if err != nil {
			fmt.Println("Failed to convert "+fields[4]+":", err)
			return
		}
		price, err := strconv.Atoi(fields[6])
		if err != nil {
			fmt.Println("Failed to convert price to int:", err)
			return
		}

		entry := Entry{User: fields[2], Volume: volume, Value: price, Type: fields[3]}
		parsed = append(parsed, entry)
	}

	aggregateList := make(map[string]*Person)
	for _, value := range parsed {
		if _, found := aggregateList[value.User]; found {
			aggregateList[value.User].Value += value.Value
			aggregateList[value.User].Volume += value.Volume
		} else {
			aggregateList[value.User] = &Person{Name: value.User, Volume: value.Volume, Value: value.Value}
		}
	}

	var totalValue int
	var totalVolume float64
	var totalTax int

	var highest *Person
	for key, value := range aggregateList {
		if highest == nil || value.Value > highest.Value {
			highest = value
		}
		totalValue += value.Value
		totalVolume += value.Volume
		totalTax += int(float64(value.Value) * TAX_RATE)
		fmt.Println(key, "; Tax due:", humanize.Commaf(float64(value.Value)*TAX_RATE), "; Total value:", humanize.Comma(int64(value.Value)), "; Total volume:", humanize.Comma(int64(value.Volume)))
	}
	fmt.Println("(Crimson Cruisers);", "Total tax:", humanize.Comma(int64(totalTax)), "; Total value:", humanize.Comma(int64(totalValue)), "; Total volume:", humanize.Comma(int64(totalVolume)))
}
